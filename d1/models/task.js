// model files should import mongoose module
const mongoose = require("mongoose");


const taskSchema = new mongoose.Schema({
	name: String,
	status:{
		type: String,
		default: "pending"
	}
});

// modules.exports = allows us to export
module.exports = mongoose.model("Task", taskSchema);

