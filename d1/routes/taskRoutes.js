// contains all endpoints for our application
// app.js/index.js should only be concerned with information about the server
// we have to use the Router method inside express
const express = require("express");
// allows us to access HTTP method middlewares that makes it easier to create routing system
const router = express.Router();

// this allows us to use the contents of "taskControllers.js" in "controllers" folder
const taskController = require("../controllers/taskController.js"); 

// route to get all tasks
/*
	get request to be sent at localhost:3000/tasks
*/
router.get("/", ( req, res ) => {
	// since the controller did not send any response, the task falls to the routes to send a response that is received from the controllers and send it to the frontend
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// miniactivity
// create "/" route that will process the request and send a response based on the createTask function inside the "taskControllers.js"
router.post("/", ( req, res ) => {
	taskController.createTask( req.body ).then(result => res.send(result));
} );

// route for deleting a task
router.delete("/:id", ( req, res ) =>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
} );

// route for updating a task
router.put("/:id", ( req, res ) =>{
	/*
		req.params.id retrieves the taskId of the document to be updated
		req.body determines what updates are to be done in the document coming from the request's body
	*/
	taskController.updateTask( req.params.id, req.body ).then(resultFromController => res.send(resultFromController));
} );

/*
	npx kill-port <port> - used when the port that should not be doing anything, is running
*/



// SECTION - Activity

router.get("/:id", (req, res) => {
	taskController.taskStatus(req.params.id).then(resultFromController => res.send(resultFromController));
});


router.put("/:id/complete", (req, res)=>{
	taskController.statusComplete(req.params.id, req.Body).then(resultFromController=> res.send(resultFromController));
});

// modules.exports = allows us to export the file where it is inserted to be used by other files such as app.js/index.js
module.exports = router;
